var socket = io();

let getCurrentTime = function() {
    let d = new Date();
    return `${d.getHours() }:${ d.getMinutes() }:${ d.getSeconds() }`;
};

// Escucha evento de conexion
socket.on('connect', function() {
    console.log('- - - - Conectado al Server - - - -');
    socket.emit('enterChat', user, function(resp) {
        user.id = resp.id;
        renderUsers(resp.people);
    });
});

// Escucha evento de desconexion
socket.on('disconnect', function() {
    console.log('(X) Conexion perdida con Server - - - -');
});

function createMessage(data) {
    socket.emit('createMessage', data, function(data) {
        if (data) {
            $('#txtMessage').val('').focus();
            renderMessages(data);
        }
    });
}

socket.on('createMessage', function(data) {
    renderMessages(data);
});

// Escuchar cuando user entra o sale del chat
socket.on('listPeople', function(data) {
    renderUsers(data);
});

// Mensajes privados
socket.on('privateMessage', function(msg) {
    console.log('Privado:', msg);
});