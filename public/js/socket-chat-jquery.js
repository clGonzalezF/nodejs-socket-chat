var searchParams = new URLSearchParams(window.location.search);
if (!searchParams.has('name') || !searchParams.has('room')) {
    window.location = 'index.html';
    throw new Error('El nombre (name) y sala (room) son necesarios');
}

var user = {
    name: searchParams.get('name'),
    room: searchParams.get('room')
};

// Funciones para render usuarios
function renderUsers(people) {
    //console.log(people);
    var html = '';
    html += '<li>';
    html += '<a href="javascript:void(0)" class="active"> Chat de <span class="room"> ' + user.room + '</span></a>';
    html += '</li>';

    for (var i in people) {
        people[i] = assignImage(people[i]);
        html += '<li>';
        html += '<a data-id="' + people[i].id + '" href="javascript:void(0)"><img src="' + people[i].img + '" alt="user-img" class="img-circle"> <span>' + people[i].name + '<small class="text-success">online</small></span></a>';
        html += '</li>';
    }

    $('#divUsuarios').html(html);
}

function renderMessages(data) {
    data = assignImage(data);
    var date = new Date(data.date);
    var time = date.getHours() + ' : ' + date.getMinutes();

    var html = '';
    if (data.id && user.id == data.id) {
        html += '<li class="animated fadeIn reverse">';
        html += '<div class="chat-content">';
        html += '<h5>' + data.name + '</h5>';
        html += '<div class="box bg-light-inverse">' + data.msg + '</div>';
        html += '</div>';
        html += '<div class="chat-img"><img src="' + data.img + '" alt="user" /></div>';
        html += '<div class="chat-time">' + time + '</div>';
        html += '</li>';
    } else {
        html += '<li class="animated fadeIn">';
        html += '<div class="chat-img"><img src="' + data.img + '" alt="user" /></div>';
        html += '<div class="chat-content">';
        html += '<h5>' + data.name + '</h5>';
        html += '<div class="box bg-light-' + ((data.name == 'Admin') ? 'success' : 'info') + '">' + data.msg + '</div>';
        html += '</div>';
        html += '<div class="chat-time">' + time + '</div>';
        html += '</li>';
    }
    $('#divChatbox').append(html);
    scrollBottom();
}

// Escuchas
$('#divUsuarios').on('click', 'a', function() {
    var id = $(this).data('id');
    if (id) {

    }
});

$('#btnSend').on('click', function(e) {
    var txt = $('#txtMessage');
    var msg = txt.val();
    if (msg) {
        createMessage(msg);
    }
    //txt.val('').focus();
});

$('#txtMessage').on('keydown', function(e) {
    if (e.keyCode == 13) {
        $('#btnSend').click();
        e.preventDefault();
    }
})








// Asignar imagenes de usuario - NO EN EL CURSO
function assignImage(people) {
    console.log(people);
    switch (people.name) {
        case 'Sakura':
            people.img = 'https://pa1.narvii.com/6762/7482d85b2df033beee6f20aa8d94cf0ccc38213a_128.gif';
            break;
        case 'Tomoyo':
            people.img = 'https://pm1.narvii.com/6255/68474cc8c2349064ee05ea78edcc03b7d03b3001_128.jpg';
            break;
        case 'Kero':
            people.img = 'https://pm1.narvii.com/6624/e33fccc80428ad467b860936acce429d5b2a262d_128.jpg';
            break;
        case 'Spinel':
            people.img = 'https://pm1.narvii.com/6462/73065fca89e0ae6cad9e7696ea26b4422cf3c227_128.jpg';
            break;
        case 'Admin':
            people.img = 'https://pa1.narvii.com/6558/494e15e377d86825421e825e5c54369e1ad18c61_128.gif';
            break;
        default:
            people.img = 'http://rechargetricks.in/wp-content/uploads/2016/09/88dadac28c13662e9665782a7a2788bd.jpg';
            break;
    }
    return people;
}