const { io } = require('../server');
const { Users } = require('../classes/users');
const users = new Users();
const { createMessage } = require('../utils/utils');

const { getCurrentTime } = require('../../myTools/currentTime');

io.on('connection', (client) => {
    client.on('enterChat', (data, callback) => {
        if (!data.name || !data.room) {
            return callback({
                error: true,
                msg: 'El nombre (name) y sala (room) son necesarios'
            });
        }

        client.join(data.room);

        users.addPerson(client.id, data.name, data.room);

        client.broadcast.to(data.room).emit('listPeople', users.getPeoplePerRoom(data.room));
        client.broadcast.to(data.room).emit('createMessage', createMessage('Admin', `${ data.name } entro a la sala`));
        callback({ id: client.id, people: users.getPeoplePerRoom(data.room) });
    });

    client.on('createMessage', (data, callback) => {
        let person = users.getPerson(client.id);
        let msg = createMessage(person.name, data);
        client.broadcast.to(person.room).emit('createMessage', msg);
        msg.id = client.id;
        callback(msg);
    });

    client.on('disconnect', () => {
        let deletedPerson = users.delPerson(client.id);
        console.log(deletedPerson);
        client.broadcast.to(deletedPerson.room).emit('createMessage', createMessage('Admin', `${ deletedPerson.name } se despide`));
        client.broadcast.to(deletedPerson.room).emit('listPeople', users.getPeoplePerRoom(deletedPerson.room));
    });

    // Mensajes privados
    client.on('privateMessage', data => {
        let person = users.getPerson(client.id);
        client.broadcast.to(data.for).emit('privateMessage', createMessage(person.name, data.msg));
    });
});