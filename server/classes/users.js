class Users {
    constructor() {
        this.people = [];
    }

    addPerson(id, name, room) {
        let people = { id, name, room };
        this.people.push(people);
        return this.people;
    }

    getPerson(id) {
        let people = this.people.filter(p => {
            return p.id === id;
        })[0];
        return people;
    }

    getPeople() {
        return this.people;
    }

    getPeoplePerRoom(room) {
        let people = this.people.filter(p => p.room === room);
        return people;
    }

    delPerson(id) {
        let deletedPerson = this.getPerson(id);
        this.people = this.people.filter(p => p.id != id);
        return deletedPerson;
    }
}

module.exports = {
    Users
}